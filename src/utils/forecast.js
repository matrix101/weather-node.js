const request = require('request')

const forecast = (latitude, longitude, callback) => {
    const url = 'https://api.darksky.net/forecast/d6ced13c7c1a747fe38394823e319fe9/'+ latitude + ',' + longitude
    request.get({url , json: true}, (error, {body}) => {
        if(error)
            callback('Unable to connect to weather service')
        else if(body.error)
            callback('Unable to find location')
        else
            callback(undefined, body.daily.data[0].summary + 'It\'s currently ' + body.currently.temperature + ' degree and there is ' + body.currently.precipProbability + '% chance of rain')
    })
}

module.exports = forecast