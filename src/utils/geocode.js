const request = require('request')

const geocode = (address, callback) => {
    const url = 'https://api.mapbox.com/geocoding/v5/mapbox.places/' + address + '.json?access_token=pk.eyJ1IjoibWF0cml4MTAxIiwiYSI6ImNrMHZjaW9pczBjZ2kzbW9jMmpjOW0xcDQifQ.0J_UkyRsvu2ZaWvIDOr7eQ&limit=1';
    request.get({ url, json: true }, (error, {body}) => {
        if (error)
            callback('Unable to connecto to location services!');
        else if (body.features.length === 0)
            callback('Unable to find location');
        else
            callback(undefined, {
                latitude: body.features[0].center[0],
                longitude: body.features[0].center[1],
                location: body.features[0].place_name
            });
    });
}

module.exports = geocode

